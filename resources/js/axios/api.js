import axios from "axios";

const API_URL = process.env.API_URL || "http://aldeliv.asistemas.com.br/api/";
const getRejectedError = error => Promise.reject(error);
const api = axios.create({
    baseURL: API_URL
});
const setConfig = config => {
    const user = localStorage.getItem("token");
    if (user) {
        config.headers.common.Authorization = `Bearer ${user}`;
    }
    return config;
};

api.interceptors.request.use(setConfig, getRejectedError);

export default api;
