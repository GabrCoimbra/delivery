export default {
    data() {
        return {
            pedidos: 1
        };
    },
    created() {
        this.$root.$on("at", () => {
            this.pedidos = this.$store.state.produtos.qtdProdutos;
        });
    }
};
