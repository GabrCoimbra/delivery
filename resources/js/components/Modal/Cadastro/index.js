import axios from "axios";
import Api from "../../../axios/api.js";
export default {
    data() {
        return {
            cadastro: null,
            ventrega: 0,
            options: [{
                text: "Selecione o estado",
                value: null
            }],
            nome: null,
            nascimento: null,
            genero: null,
            cpf: null,
            email: null,
            senha: null,
            confirmasenha: null,
            ddd: '13',
            telefone: null,
            cep: null,
            estado: null,
            cidade: null,
            bairro: null,
            endereco: null,
            numero: null,
            complemento: null
        };
    },
    created() {
        this.$root.$on("open-cadastro", produto => {
            $("body,html").css({
                overflow: "hidden"
            });
            this.cadastro = true;
        });
    },
    methods: {
        fechar() {
            $("body,html").css({
                "overflow-y": "auto"
            });
            this.cadastro = false;
        },
        validaCpf(cpf) {
            if (cpf && cpf.length == 11) {
                var f = true;
                if (
                    !cpf ||
                    cpf.length != 11 ||
                    cpf == "00000000000" ||
                    cpf == "11111111111" ||
                    cpf == "22222222222" ||
                    cpf == "33333333333" ||
                    cpf == "44444444444" ||
                    cpf == "55555555555" ||
                    cpf == "66666666666" ||
                    cpf == "77777777777" ||
                    cpf == "88888888888" ||
                    cpf == "99999999999"
                ) {
                    f = false;
                }
                var soma = 0;
                var resto;
                for (var i = 1; i <= 9; i++) {
                    soma = soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
                }
                resto = (soma * 10) % 11;
                if (resto == 10 || resto == 11) {
                    resto = 0;
                }
                if (resto != parseInt(cpf.substring(9, 10))) {
                    f = false;
                }
                soma = 0;
                for (var i = 1; i <= 10; i++) {
                    soma = soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
                }
                resto = (soma * 10) % 11;
                if (resto == 10 || resto == 11) {
                    resto = 0;
                }

                if (resto != parseInt(cpf.substring(10, 11))) {
                    f = false;
                }
                if (f) {
                    $("#input-nascimento").focus();
                    $("#input-cpf").css("border", "3px solid green");
                    $("#button-enviar").attr("disabled", false);
                } else {
                    $("#input-cpf").css("border", "3px solid red");
                    $("#button-enviar").attr("disabled", true);
                }
            }
        },
        maxLenght(event) {
            const value = event.target.value;
            if (String(value).length < 7) {
                this.cep = value;
            } else {
                var oi = this.cep.slice(0, 8);
                this.cep = oi;
                this.buscaCEP();
            }
        },
        buscaCEP() {
            if (this.cep.length == 8) {
                axios
                    .get(`https://viacep.com.br/ws/${this.cep}/json/`)
                    .then(response => {
                        this.cidade = response.data.localidade;
                        this.estado = response.data.uf;
                        this.bairro = response.data.bairro;
                        this.endereco = response.data.logradouro;
                    });
            }
        },
        enviar() {
            Api.post(`/buscaentrega`, {
                cidade: this.cidade,
                estado: this.estado,
                cep: this.cep
            }).then(buscagoogle => {
                this.ventrega = buscagoogle.data.mensagem;
                if (this.ventrega == 1) {
                    $(".login-input, .select-cadastro").css("border", "1px solid #ccc");
                    if (
                        this.nome != null &&
                        this.telefone != null &&
                        this.estado != null &&
                        (this.cidade != null || this.cidade == "") &&
                        (this.bairro != null || this.bairro == "") &&
                        (this.endereco != null || this.endereco == "") &&
                        this.numero != null
                    ) {
                        var dados = [
                            this.nome,
                            this.nascimento,
                            this.genero,
                            this.cpf,
                            this.email,
                            this.telefone,
                            this.cep,
                            this.estado,
                            this.cidade,
                            this.bairro,
                            this.endereco,
                            this.numero,
                            this.complemento,
                            this.ddd
                        ];
                        $("#button-enviar").attr("disabled", true);
                        Api.post(`/cadastro`, {
                            dados
                        }).then(response => {
                            if (!response.data.error) {
                                localStorage.token = response.data.retorno.token;
                                localStorage.usuario = response.data.retorno.usuario;
                                localStorage.cpf = response.data.retorno.cpf;
                                localStorage.telefone = response.data.retorno.telefone;
                                localStorage.nome = response.data.retorno.nome;
                                localStorage.distancia = response.data.retorno.distancia;
                                this.$store.commit(
                                    "mudadistancia",
                                    response.data.retorno.distancia
                                );
                                var enviar = {
                                    nome: response.data.retorno.nome,
                                    cpf: response.data.retorno.cpF
                                };
                                this.$store.commit("mudalogin", enviar);
                                $("body,html").css({
                                    "overflow-y": "auto"
                                });
                                alert("Sucesso ao cadastrar: " + this.nome);
                                $("body,html").css({
                                    "overflow-y": "auto"
                                });
                                this.cadastro = false;
                                $(".menu-icon").css("display", "none");
                                if (this.$store.state.pag == "ConfirmaPedido") {
                                    this.$root.$emit("open-modal-4");
                                }
                            } else {
                                alert(response.data.error);
                            }
                        });
                    }
                } else {
                    alert("Área não atendida")
                }
            })
        },
        verifica: function () {
            let i = null
            if (
                this.estado != null &&
                this.cidade != null &&
                this.bairro != null &&
                this.endereco != null
            ) {
                Api.post(`/buscaentrega`, {
                    cidade: this.cidade,
                    estado: this.estado,
                    cep: this.cep
                }).then(buscagoogle => {
                    this.ventrega = null
                    this.ventrega = buscagoogle.data.mensagem;
                    if (this.ventrega == 0) {
                        i = '0'
                    } else {
                        i = '1'
                    }
                });

            }
            return this.ventrega
        },

        verificaLogin() {
            Api.post("login", {
                Login: this.ddd + this.telefone,
                Url: "cadastro"
            }).then(response => {
                if (!response.data.msg) {
                    localStorage.token = response.data.retorno.token;
                    localStorage.usuario = response.data.retorno.usuario;
                    localStorage.cpf = response.data.retorno.cpf;
                    localStorage.telefone = response.data.retorno.telefone;
                    localStorage.nome = response.data.retorno.nome;
                    localStorage.distancia = response.data.retorno.distancia;
                    this.$store.commit(
                        "mudadistancia",
                        response.data.retorno.distancia
                    );
                    var enviar = {
                        nome: response.data.retorno.nome,
                        cpf: response.data.retorno.cpF
                    };
                    this.$store.commit("mudalogin", enviar);
                    $("body,html").css({
                        "overflow-y": "auto"
                    });
                    this.cadastro = false;
                    if (this.$store.state.pag == "ConfirmaPedido") {
                        this.$root.$emit("open-modal-4");
                    }
                    alert("Cadastro encontrado");
                }
            });
        }

    },
    mounted() {
        this.verifica();
    }
};
