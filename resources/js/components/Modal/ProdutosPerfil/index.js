import Api from "../../../axios/api.js";

export default {
    data() {
        return {
            pedido: null,
            modalpedidos: false,
            itens: null,
            total: 0.0,
            tp: 0.0,
        };
    },
    created() {
        this.$root.$on("open-pedidos", pedido => {
            $('body,html').css({
                "overflow": "hidden"
            })
            this.pedido = pedido
            this.modalpedidos = true;
            Api.post('produtospedido', {
                    pedido: pedido.idPedido
                })
                .then(response => {
                    this.itens = response.data.lista
                })
        });
    },
    computed: {
        funcTotal: function () {
            this.total = '0.0'
            let maior;
            if (this.itens != null) {
                var p = this.itens.map(produto => {
                    if (produto.qtdProduto == 0.5) {
                        if (this.$store.state.especial != 1) {
                            if (maior != null) {
                                if (maior.PrcVenda < produto.PrcVenda) {
                                    maior = produto
                                }
                                this.total =
                                    parseFloat(this.total) +
                                    parseFloat(produto.PrcVenda) * 1;
                            } else {
                                maior = produto

                            }
                        } else {
                            this.total =
                                parseFloat(this.total) +
                                parseFloat(produto.PrcVenda) * 0.5;

                        }
                    } else {
                        if (produto.idAdicional) {
                            if (this.$store.state.adc == 1) {
                                this.total =
                                    parseFloat(this.total) +
                                    parseFloat(produto.PrcVenda) * 0.5;
                            } else {
                                this.total =
                                    parseFloat(this.total) +
                                    parseFloat(produto.PrcVenda) * 1;
                            }
                        } else {
                            this.total =
                                parseFloat(this.total) +
                                parseFloat(produto.PrcVenda) * produto.qtdProduto;
                        }
                    }
                    // Se existe adicional

                });
            }
            this.total = this.total - this.pedido.ValDesconto
            return parseFloat(this.total).toLocaleString("pt-BR", {
                style: "currency",
                currency: "BRL"
            });
        },
        produtos: function () {
            this.tp = '0.0'
            let maior;
            if (this.itens != null) {
                var p = this.itens.map(produto => {
                    if (produto.qtdProduto == 0.5) {
                        if (this.$store.state.especial != 1) {
                            if (maior != null) {
                                if (maior.PrcVenda < produto.PrcVenda) {
                                    maior = produto
                                }
                                this.tp =
                                    parseFloat(this.tp) +
                                    parseFloat(produto.PrcVenda) * 1;
                            } else {
                                maior = produto

                            }
                        } else {
                            this.tp =
                                parseFloat(this.tp) +
                                parseFloat(produto.PrcVenda) * 0.5;

                        }
                    } else {
                        if (produto.idAdicional) {
                            if (this.$store.state.adc == 1) {
                                this.tp =
                                    parseFloat(this.tp) +
                                    parseFloat(produto.PrcVenda) * 0.5;
                            } else {
                                this.tp =
                                    parseFloat(this.tp) +
                                    parseFloat(produto.PrcVenda) * 1;
                            }
                        } else {
                            this.tp =
                                parseFloat(this.tp) +
                                parseFloat(produto.PrcVenda) * produto.qtdProduto;
                        }
                    }
                    // Se existe adicional

                });
            }
            return parseFloat(this.tp).toLocaleString("pt-BR", {
                style: "currency",
                currency: "BRL"
            });
        }
    },
    methods: {
        fechar() {
            $('body,html').css({
                "overflow-y": "auto"
            })
            this.modalpedidos = false;
            this.itens = null
        }
    }
};
