import Api from "../../../axios/api.js";
export default {
    data() {
        return {
            visible: false,
            produto: null,
            metades: null,
            produtoMetade: null,
            produtoSelecionado: null,
            obs1: null,
            adicional1: null,
            obs2: null,
            adicional2: null,
            pt: true,
            pt1: null,
            pt2: null,
            pt3: null,
            obstmp1: new Array(),
            st: false
        };
    },
    created() {
        this.$root.$on("open-metade", produto => {
            this.pt = true
            $('body,html').css({
                "overflow": "hidden"
            })
            this.visible = true;
            Api.post('infprodutos', {
                    produto: produto.idProduto
                })
                .then(response => {
                    this.pt = false
                    this.pt1 = true
                    if (response.data.adc.length != 0) {
                        response.data.adc.map(produto => {
                            produto.Contagem = 0
                        })
                        this.adicional1 = response.data.adc
                    } else {
                        this.adicional1 = null
                    }
                    if (response.data.obs.length != 0) {
                        this.obs1 = response.data.obs
                    }

                })
            this.produto = produto;
            Api.post('buscametades', {
                    produto: this.produto.idProduto,
                    familia: this.produto.Familia
                })
                .then(response => {
                    this.metades = response.data.lista
                })
        });

        if (this.produtoMetade != null) {

        }
    },
    computed: {
        nome: function () {
            if (this.metades) {
                this.metades.map(i => {
                    if (i.idProduto == this.produtoMetade) {
                        this.produtoSelecionado = i
                    }
                })
                if (this.produtoMetade != null) {

                    return this.produtoSelecionado.NomeProd
                }
            }
        },

        funcTotal: function () {
            var total = '0.0'
            if (this.pt3) {
                if (this.produto.adc) {
                    this.produto.adc.map(produto => {
                        if (this.$store.state.adc == 1) {
                            total = parseFloat(total) + parseFloat(((parseFloat(produto.preco) * produto.qtd) * 0.5))
                        } else {
                            total = parseFloat(total) + parseFloat(((parseFloat(produto.preco) * produto.qtd)))
                        }
                    });

                }
                if (this.produto && this.produtoSelecionado) {
                    if (this.$store.state.especial == 0) {
                        if (this.produtoSelecionado.PrcVenda >= this.produto.PrcVenda) {
                            total = parseFloat(total) + parseFloat(this.produtoSelecionado.PrcVenda * 1)
                        } else {
                            total = parseFloat(total) + parseFloat(this.produto.PrcVenda * 1)
                        }
                    } else {
                        total = parseFloat(total) + ((parseFloat(this.produto.PrcVenda) + parseFloat(this.produtoSelecionado.PrcVenda)) * 0.5)
                    }
                    total = total.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    });
                }
                return total;
            }
        }

    },
    methods: {

        fechar() {
            $('body,html').css({
                "overflow-y": "auto"
            })
            this.visible = false;
            this.produto = null
            this.produtoSelecionado = null
            this.metades = null
            this.produtoMetade = null
            this.produtoSelecionado = null
            this.obs1 = null
            this.adicional1 = null
            this.obs2 = null
            this.adicional2 = null
            this.obstmp1 = []
            this.pt1 = null
            this.pt2 = null
            this.pt3 = null
            this.st = false
        },
        addObs1(tipo) {
            var obs = null;
            if (tipo == 2) {
                $(".inputs input").each(function (index, element) {
                    if (element.value !== "") {
                        obs = {
                            id: null,
                            nome: element.value
                        };
                        element.focus()
                        element.value = ""
                    }
                });
            }
            if (tipo == 1) {
                var ObsItem = $(".select")[0]
                if (ObsItem) {
                    $(".select").each(function (index, element) {
                        if (element.value !== "") {
                            ObsItem = element.value
                            var a = {
                                id: ObsItem.split("-")[0],
                                nome: ObsItem.split("-")[1]
                            };
                            obs = a;
                        }
                    });
                }
            }
            if (obs) {
                var v = true
                this.obstmp1.map(i => {
                    if (i.nome.toUpperCase() == obs.nome.toUpperCase()) {
                        alert('Observação já adicionada')
                        v = false
                    }
                })
                if (v) {
                    this.obstmp1.push(obs)
                }
                obs = null;
            }
        },
        removeObs(obs) {
            var index = this.obstmp1.findIndex(
                o => o.nome == obs
            );
            this.obstmp1.splice(index, 1);
        },
        add1(p) {
            this.adicional1.map(produto => {
                if (produto.idProduto == p.idProduto) {
                    produto.Contagem = produto.Contagem + 1
                }
            })

        },
        rtr1(p) {
            this.adicional1.map(produto => {
                if (produto.idProduto == p.idProduto) {
                    if (produto.Contagem > 0) {
                        produto.Contagem = produto.Contagem - 1
                    }
                }
            })

        },
        add2(p) {
            this.adicional2.map(produto => {
                if (produto.idProduto == p.idProduto) {
                    produto.Contagem = produto.Contagem + 1
                }
            })

        },
        rtr2(p) {
            this.adicional2.map(produto => {
                if (produto.idProduto == p.idProduto) {
                    if (produto.Contagem > 0) {
                        produto.Contagem = produto.Contagem - 1
                    }
                }
            })

        },
        irPt2() {
            var i = new Array()
            if (this.adicional1) {
                this.adicional1.map(produto => {
                    if (produto.Contagem >= 1) {
                        i.push({
                            id: produto.idProduto,
                            preco: produto.PrcVenda,
                            nome: produto.NomeProd,
                            qtd: produto.Contagem
                        })
                    }
                })
                if (i.length > 0) {
                    this.produto = Object.assign(this.produto, {
                        adc: i
                    });
                    i = null
                }
            }
            if (this.obstmp1.length > 0) {
                this.produto = Object.assign(this.produto, {
                    obs: this.obstmp1
                });
            }
            this.obstmp1 = null;
            this.obstmp1 = new Array();
            this.pt1 = false;
            this.pt2 = true;
        },
        irPt3() {
            if (this.produtoSelecionado) {
                this.pt2 = false
                this.pt = true
                if (this.produtoSelecionado != null) {
                    Api.post('infprodutos', {
                            produto: this.produtoSelecionado.idProduto
                        })
                        .then(response => {
                            this.pt3 = true
                            this.pt = false
                            if (response.data.adc.length != 0) {
                                response.data.adc.map(produto => {
                                    produto.Contagem = 0
                                })
                                this.adicional2 = response.data.adc
                            } else {
                                this.adicional2 = null
                            }
                            if (response.data.obs.length != 0) {
                                this.obs2 = response.data.obs
                            }
                        })
                }
            } else {
                alert('Selecione um produto')
            }
        },
        adicionarMetade() {
            // Aqui fará a logica de pegar observações e adicionais do 2 produto
            var i = new Array()
            if (this.adicional2) {
                this.adicional2.map(produto => {
                    if (produto.Contagem >= 1) {
                        i.push({
                            id: produto.idProduto,
                            preco: produto.PrcVenda,
                            nome: produto.NomeProd,
                            qtd: produto.Contagem
                        })
                    }
                })
                if (i.length > 0) {

                    this.produtoSelecionado = Object.assign(this.produtoSelecionado, {
                        adc: i
                    });
                }
            }
            if (this.obstmp1.length > 0) {
                this.produtoSelecionado = Object.assign(this.produtoSelecionado, {
                    obs: this.obstmp1
                });
            }

            //
            var qtdmetades = this.$store.state.qtdMetades + 1;
            var id1 = this.$store.state.qtdProdutos + 1;
            this.produto.id = id1
            this.produto.Contagem = 0.5
            this.produto.idPrdEspecial = qtdmetades
            this.produtoSelecionado.id = id1 + 1
            this.produtoSelecionado.Contagem = 0.5
            this.produtoSelecionado.idPrdEspecial = qtdmetades
            if (this.produtoSelecionado.PrcVenda >= this.produto.PrcVenda) {
                this.produtoSelecionado.itemMaior = 1
            } else {
                this.produto.itemMaior = 1
            }
            this.$store.commit("adicionarMetade", this.produto);
            this.$store.commit("adicionarMetade", this.produtoSelecionado);
            this.$store.commit("addNumMetade");
            $('body,html').css({
                "overflow-y": "auto"
            })
            this.produto = null
            this.produtoSelecionado = null
            this.metades = null
            this.produtoMetade = null
            this.produtoSelecionado = null
            this.obs1 = null
            this.adicional1 = null
            this.obs2 = null
            this.adicional2 = null
            this.pt1 = null
            this.pt2 = null
            this.pt3 = null
            this.st = false
            this.$router.push('/confirmapedido')
        }
    }
};
