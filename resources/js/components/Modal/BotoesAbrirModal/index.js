import Api from "../../../axios/api.js";

export default {
    data() {
        return {
            modal: false,
            produto: null
        };
    },
    created() {
        this.$root.$on("open-botoesModal", produto => {
            this.modal = true
            this.produto = produto
        })

    },
    methods: {
        fechar() {
            $('body,html').css({
                "overflow-y": "auto"
            })
            this.modal = false;
        }
    }
};
