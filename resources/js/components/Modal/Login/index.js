import Api from "../../../axios/api.js";
export default {
    data() {
        return {
            login: false,
            ddd: '13',
            cpf: null,
            senha: null,
            error: null
        };
    },
    created() {
        $(".menu-icon").css("display", "none");
        this.$root.$on("open-login", produto => {
            $("body,html").css({
                overflow: "hidden"
            });
            this.login = true;
        });
    },
    methods: {
        fechar() {
            $("body,html").css({
                "overflow-y": "auto"
            });
            this.login = false;
        },
        abrirCadastro() {
            this.login = false;
            this.$root.$emit("open-cadastro");
        },
        calculatx(tx, d) {
            var a = 0;
            var i = false;
            tx.map(p => {
                if (p.DistanciaMetros == 0) {
                    a = p.Taxa;
                    i = true;
                } else {
                    if (!i) {
                        if (p.DistanciaMetros < d) {
                            a = p.Taxa;
                        }
                    }
                }
            });
            return a;
        },
        entrar() {
            this.error = null;
            Api.post("login", {
                    Login: this.ddd + this.cpf
                })
                .then(response => {
                    if (!response.data.msg) {
                        localStorage.token = response.data.retorno.token;
                        localStorage.usuario = response.data.retorno.usuario;
                        localStorage.cpf = response.data.retorno.cpf;
                        localStorage.telefone = response.data.retorno.telefone;
                        localStorage.nome = response.data.retorno.nome;
                        localStorage.distancia =
                            response.data.retorno.distancia;
                        this.$store.commit(
                            "mudadistancia",
                            response.data.retorno.distancia
                        );
                        Api.get("taxaentrega").then(p => {
                            localStorage.taxa = this.calculatx(p.data.lista);
                        });
                        var enviar = {
                            nome: response.data.retorno.nome,
                            cpf: response.data.retorno.cpF
                        };
                        this.$store.commit("mudalogin", enviar);
                        $("body,html").css({
                            "overflow-y": "auto"
                        });

                        this.login = false;
                    } else {
                        this.error = response.data.msg;
                    }
                })
                .catch(e => {
                    this.error = e;
                });
        }
    }
};
