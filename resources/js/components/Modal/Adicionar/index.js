import Api from "../../../axios/api.js";
export default {
    data() {
        return {
            visible: false,
            produto: null,
            adicional: null,
            obs: null,
            obstmp1: new Array(),
            pt: null,
            pt1: false
        };
    },
    created() {
        this.$root.$on("open-adicionar", produto => {
            $("body,html").css({
                overflow: "hidden"
            });
            this.visible = true;
            this.produto = produto;
            this.produto.Contagem = 1;
            this.pt = true;
            this.visible = true;
            Api.post("infprodutos", {
                produto: produto.idProduto
            }).then(
                response => {
                    this.pt = false;
                    this.pt1 = true;
                    if (response.data.adc.length != 0) {
                        response.data.adc.map(produto => {
                            produto.PrcVenda = parseFloat(produto.PrcVenda);
                            produto.Contagem = 0;
                        });
                        this.adicional = response.data.adc;
                    } else {
                        this.adicional = null;
                    }
                    if (response.data.obs.length != 0) {
                        this.obs = response.data.obs;
                    }
                }
            );
        });
    },
    methods: {
        fechar() {
            $("body,html").css({
                "overflow-y": "auto"
            });
            this.visible = false;
            this.obstmp1 = [];
            this.pt1 = null;
        },
        add(p) {
            this.adicional.map(produto => {
                if (produto.idProduto == p.idProduto) {
                    produto.Contagem = produto.Contagem + 1;
                }
            });
        },
        rtr(p) {
            this.adicional.map(produto => {
                if (produto.idProduto == p.idProduto) {
                    if (produto.Contagem > 0) {
                        produto.Contagem = produto.Contagem - 1;
                    }
                }
            });
        },
        addObs1(tipo) {
            var obs = null;
            if (tipo == 2) {
                $(".inputs input").each(function (index, element) {
                    if (element.value !== "") {
                        obs = {
                            id: null,
                            nome: element.value
                        };
                    }
                    $("#input-obs")[0].value = "";
                    $("#input-obs")[0].focus();
                });
            }
            if (tipo == 1) {
                var ObsItem = $(".select")[0];
                if (ObsItem) {
                    $(".select").each(function (index, element) {
                        if (element.value !== "") {
                            ObsItem = element.value;
                            var a = {
                                id: ObsItem.split("-")[0],
                                nome: ObsItem.split("-")[1]
                            };
                            obs = a;
                        }
                    });
                }
            }
            if (obs) {
                var v = true;
                this.obstmp1.map(i => {
                    if (i.nome.toUpperCase() == obs.nome.toUpperCase()) {
                        alert("Observação já adicionada");
                        v = false;
                    }
                });
                if (v) {
                    this.obstmp1.push(obs);
                }
                obs = null;
            }
        },
        removeObs(obs) {
            var index = this.obstmp1.findIndex(o => o.nome == obs);
            this.obstmp1.splice(index, 1);
        },
        adicionar() {
            this.produto.Contagem = this.produto.Contagem + 1;
        },
        retirar() {
            if (this.produto.Contagem > 0) {
                this.produto.Contagem = this.produto.Contagem - 1;
            }
        },
        enviar() {
            var i = new Array();
            if (this.adicional) {
                this.adicional.map(produto => {
                    if (produto.Contagem >= 1) {
                        i.push({
                            id: produto.idProduto,
                            preco: produto.PrcVenda,
                            nome: produto.NomeProd,
                            qtd: produto.Contagem
                        });
                    }
                });
                if (i.length > 0) {
                    this.produto = Object.assign(this.produto, {
                        adc: i
                    });
                }
            }
            if (this.obstmp1.length > 0) {
                this.produto = Object.assign(this.produto, {
                    obs: this.obstmp1
                });
            }
            var id = this.$store.state.qtdProdutos + 1;
            this.produto = Object.assign(this.produto, {
                id: id
            });
            this.$store.commit("adicionarProduto", this.produto);
            this.produto = null;
            this.obs = null;
            this.adicional = null;
            this.visible = false;
            this.obstmp1 = [];
            $("body,html").css({
                "overflow-y": "auto"
            });
        },
        criarSelect() {
            $("#div-selects").append($("#select").clone());
        },
        criarinput() {
            $(".inputs").append('<input class="col-10" type="text" />');
        }
    }
};
