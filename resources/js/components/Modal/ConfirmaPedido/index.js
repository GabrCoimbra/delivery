import Api from "../../../axios/api.js";
export default {
    data() {
        return {
            visible4: false,
            FormaPagto: null,
            tpcartao: null,
            bandeira: null,
            total: null,
            Troco: "0.0",
            EnvTroco: "0.0",
            Cpf: null,
            entrega: null,
            CPFNota: this.$store.state.cpf,
            cupom: null,
            campocupom: null,
            idcupom: null,
            desconto: 0,
            tx: null,
            buscaCupomFinalizada: null,
            tpcartao: null,
            bandeira: null
        };
    },
    created() {
        this.$root.$on("open-modal-4", p => {
            this.tx = p;
            $("body,html").css({
                overflow: "hidden"
            });
            this.visible4 = true;
        });
        this.buscaCartoes();
    },
    computed: {
        txentrega: function () {
            this.tx = this.$store.state.taxa;
            return parseFloat(this.$store.state.taxa);
        },
        funcTotal: function () {
            this.total = "0.0";
            var p = this.$store.state.pedidos.map(produto => {
                if (produto.qtd == 0.5) {
                    if (this.$store.state.especial == 0) {
                        if (produto.itemMaior == 1) {
                            this.total =
                                parseFloat(this.total) +
                                parseFloat(produto.preco) * 1;
                        }
                    } else {
                        this.total =
                            parseFloat(this.total) +
                            parseFloat(produto.preco) * 0.5;
                    }
                } else {
                    this.total =
                        parseFloat(this.total) +
                        parseFloat(produto.preco) * produto.qtd;
                }
                // Se existe adicional
                if (produto.adc) {
                    produto.adc.map(a => {
                        if (produto.qtd != 0.5) {
                            this.total =
                                this.total + parseFloat(a.preco) * a.qtd;
                        } else {
                            if (this.$store.state.adc == 1) {
                                this.total =
                                    parseFloat(this.total) +
                                    parseFloat(
                                        parseFloat(a.preco) * a.qtd * 0.5
                                    );
                            } else {
                                this.total =
                                    parseFloat(this.total) +
                                    parseFloat(parseFloat(a.preco) * a.qtd * 1);
                            }
                        }
                    });
                }
            });
            if (this.entrega == 1) {
                this.total = parseFloat(this.total) + this.txentrega;
            }
            this.total = this.total - this.desconto;
            return parseFloat(this.total).toLocaleString("pt-BR", {
                style: "currency",
                currency: "BRL"
            });
        }
    },
    methods: {
        fechar() {
            $("body,html").css({
                "overflow-y": "auto"
            });
            this.visible4 = false;
        },
        buscaCartoes() {
            var c = [];

            Api.get("buscacartoes").then(response => {
                response.data.lista.map(r => {
                    c.push({
                        nome: r.Bandeira,
                        value: r.Bandeira
                    });

                });
            });
            this.tpcartao = c;
        },
        enviar: function () {
            const intervalId = setInterval(() => {
                if (this.cupom != "" && this.desconto == null) {
                    if (this.buscaCupomFinalizada) {
                        clearInterval(intervalId);
                        efetuarEnvio();
                    }
                } else {
                    clearInterval(intervalId);
                    efetuarEnvio();
                }
            }, 1000);


            const efetuarEnvio = () => {
                $("#btn-pedido-v").attr("disabled", true);
                var verificacao = true;
                if (this.FormaPagto != null && this.Cpf != null) {
                    if (this.Cpf == 1 && this.CPFNota == null) {
                        $("#btn-pedido-v").attr("disabled", false);
                        alert("Digite o CPF");
                        $("#input-cpf").focus();
                        $("#input-cpf").css("border", "3px solid red");
                        verificacao = false;
                    }
                    if (this.Cpf == 0) {
                        this.CPFNota = null;
                    }
                    if (this.FormaPagto == 0) {
                        if (parseFloat(this.Troco) != 0.0) {
                            if (
                                parseFloat(this.Troco) < parseFloat(this.total)
                            ) {
                                verificacao = false;
                                $("#btn-pedido-v").attr("disabled", false);
                                alert("Troco menor que o total");
                            } else {
                                this.EnvTroco =
                                    parseFloat(this.Troco) -
                                    parseFloat(this.total);
                            }
                        }
                    }
                    console.log(this.bandeira)
                    if (verificacao) {
                        if (this.$store.state.qtdProdutos > 0) {
                            Api.post("enviaPedido", {
                                data: this.$store.state,
                                usuario: localStorage.usuario,
                                entrega: this.entrega,
                                FormaPagto: this.FormaPagto,
                                Troco: this.EnvTroco,
                                CPFNota: this.CPFNota,
                                txentrega: this.txentrega,
                                desconto: this.desconto,
                                cupom: this.idcupom,
                                bandeira: this.bandeira

                            }).then(response => {
                                $("#btn-pedido-v").attr("disabled", false);
                                this.visible4 = false;
                                if (response.data.msg) {
                                    alert(response.data.msg);
                                    window.location = "/meuspedidos";
                                } else {
                                    alert(response.data.error);
                                }
                            });
                        } else {
                            alert("Coloque produtos no carrinho");
                        }
                    } else {
                        $("#btn-pedido-v").attr("disabled", false);
                    }
                } else {
                    $("#btn-pedido-v").attr("disabled", false);
                    alert(
                        "Selecione a forma de pagamento e se deseja cpf na nota"
                    );
                }
            };
        },
        // FUNÇÃO DO CUPOM
        verificarCupom: function () {
            this.buscaCupomFinalizada = null;
            if (this.cupom != null) {
                Api.post("buscacupom", {
                        cupom: this.cupom,
                        id: localStorage.usuario
                    })
                    .then(response => {
                        // RESPOSTA DE ERRO
                        if (response.data.error) {
                            alert(response.data.error);
                            this.campocupom = null;
                            this.cupom = ""
                        } else {
                            //RESPOSTA DE SUCESSO AO ENCONTRAR O CUPOM
                            if (response.data.cupom) {
                                var cupom = response.data.cupom;
                                var p = this.funcTotal.split("R$");
                                if (
                                    parseFloat(p[1]) >
                                    parseFloat(cupom.ValMinVenda)
                                ) {
                                    if (cupom.idModatipo == 1) {
                                        var d = cupom.Desconto;
                                    }
                                    if (cupom.idModatipo == 2) {
                                        var preco = this.precoprodutos();
                                        var d =
                                            parseFloat(preco) *
                                            parseFloat(cupom.Desconto * 0.01);
                                    }
                                    this.campocupom = this.cupom;
                                    this.desconto = d;
                                    this.idcupom = cupom.idPromocao;
                                } else {
                                    var erro =
                                        "Cupom válido para valor mínimo de R$" +
                                        cupom.ValMinVenda;
                                    this.cupom = ""
                                    alert(erro);
                                }
                            }
                        }
                    })
                    .finally(() => (this.buscaCupomFinalizada = true));
            } else {}
        },
        removeCupom() {
            this.campocupom = null;
            this.desconto = 0;
        },
        precoprodutos() {
            var total = 0;
            var p = this.$store.state.pedidos.map(produto => {
                if (produto.qtd == 0.5) {
                    if (this.$store.state.especial == 0) {
                        if (produto.itemMaior == 1) {
                            total =
                                parseFloat(total) +
                                parseFloat(produto.preco) * 1;
                        }
                    } else {
                        total =
                            parseFloat(total) + parseFloat(produto.preco) * 0.5;
                    }
                } else {
                    total =
                        parseFloat(total) +
                        parseFloat(produto.preco) * produto.qtd;
                }
                // Se existe adicional
                if (produto.adc) {
                    produto.adc.map(a => {
                        if (produto.qtd != 0.5) {
                            total = total + parseFloat(a.preco) * a.qtd;
                        } else {
                            if (this.$store.state.adc == 1) {
                                total =
                                    parseFloat(total) +
                                    parseFloat(
                                        parseFloat(a.preco) * a.qtd * 0.5
                                    );
                            } else {
                                total =
                                    parseFloat(total) +
                                    parseFloat(parseFloat(a.preco) * a.qtd * 1);
                            }
                        }
                    });
                }
            });
            return total;
        }
    }
};
