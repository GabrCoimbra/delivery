import Login from "../Modal/Login/Login.vue";
import Cadastro from "../Modal/Cadastro/Cadastro.vue";
import Api from "../../axios/api.js";
import {
    mapState
} from "vuex";
const Header = {
    name: "Header",
    data: function () {
        return {
            total: 0.0,
            horarioF: false,
            tempoEntrega: null
        };
    },
    components: {
        Login,
        Cadastro
    },
    computed: {
        pag: function () {
            return this.$store.state.pag;
        },
        verifica: function () {
            if (this.$store.state.vlogin) {
                var nome = this.$store.state.vlogin.split(" ")
                return nome[0];
            } else {

                return null;
            }
        },
        vpedidos: function () {
            this.pag = this.$router.currentRoute.name;
            return this.$store.state.pedidos.length;
        },
        funcTotal: function () {
            this.total = "0.0";
            var p = this.$store.state.pedidos.map(produto => {
                if (produto.qtd == 0.5) {
                    if (produto.itemMaior == 1) {
                        this.total =
                            parseFloat(this.total) +
                            parseFloat(produto.preco) * 1;
                    }
                } else {
                    this.total =
                        parseFloat(this.total) +
                        parseFloat(produto.preco) * produto.qtd;
                }
                // Se existe adicional
                if (produto.adc) {
                    produto.adc.map(a => {
                        if (produto.qtd != 0.5) {
                            this.total =
                                this.total + parseFloat(a.preco) * a.qtd;
                        } else {
                            this.total =
                                parseFloat(this.total) +
                                parseFloat(parseFloat(a.preco) * a.qtd * 0.5);
                        }
                    });
                }
            });
            return parseFloat(this.total).toLocaleString("pt-BR", {
                style: "currency",
                currency: "BRL"
            });
        }
    },
    created() {
        Api.get("tipocobranca").then(response => {
            this.$store.commit("mudarCobranca", response.data.lista);
        })
        Api.get("hrentrega").then(response => {
            this.tempoEntrega = response.data.lista.Valor
        })
        Api.get("horarios").then(response => {
            var i = "";
            if (response.data.lista.length != 0) {
                var data = new Date();
                var h = String(data.getHours())
                h = ("0" + h).slice(-2);
                h = h + data.getMinutes(); // 0-23 // 0-59
                response.data.lista.map((item, indice) => {
                    if (indice == 0) {
                        var hora = item.HoraInicio.split(":");
                        item.HoraInicio = hora[0];
                        item.HoraInicio = item.HoraInicio + ":" + hora[1];
                        i = " <span style='display:block'> das " + i + item.HoraInicio;
                        var hora1 = item.HoraFinal.split(":");
                        item.HoraFinal = hora1[0];
                        item.HoraFinal = item.HoraFinal + ":" + hora1[1];
                        i = i + " às " + item.HoraFinal + '</span>';
                        var fim = item.HoraFinal.split(":");
                        if (h >= hora[0] + hora[1] && h < fim[0] + fim[1]) {
                            this.horarioF = true;
                        }
                    } else {
                        var hora1 = item.HoraInicio.split(":");
                        item.HoraInicio = hora1[0];
                        item.HoraInicio = item.HoraInicio + ":" + hora1[1];
                        i = i + "<span> das " + item.HoraInicio;

                        var hora = item.HoraFinal.split(":");
                        item.HoraFinal = hora[0];
                        item.HoraFinal = item.HoraFinal + ":" + hora[1];
                        i = i + "  às " + item.HoraFinal + '</span>';

                        var inicio = item.HoraInicio.split(":");
                        if (
                            h >= inicio[0] + inicio[1] &&
                            h < hora[0] + hora[1]
                        ) {
                            this.horarioF = true;
                        }
                    }
                });
                this.$store.commit("mudastatus", this.horarioF);
            } else {
                i = "Hoje não abre";
                this.horarioF = "hoje não abre";
                this.$store.commit("mudastatus", false);
            }
            $("#horario-funcionamento-txt").html(i);
        });
    },
    methods: {
        irPagina() {
            if (this.pag == "MeusPedidos") {
                window.location = "/";
            } else {
                this.$router.push('/confirmapedido')
            }
        },
        abreMenu() {
            var a = $(".menu-hamburg").attr("data-state");
            if (a == 0) {
                $(".redboa-menu-fixed").toggle(0);
                a = 1;
            } else {
                $(".redboa-menu-fixed").toggle(700);
                a = 0;
            }
        }
    }
};
export default Header;
