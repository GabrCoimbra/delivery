import Metade from "../../components/Modal/Metade/Metade.vue";
import Adicionar from "../../components/Modal/Adicionar/Adicionar.vue";
import Botoes from "../../components/Modal/BotoesAbrirModal/BotoesAbrirModal.vue";
import Api from "../../axios/api.js";
const Categoria = {
    name: "Categoria",
    data: function() {
        return {
            todos: [],
            familias: [],
            pedidos: null,
            categoria: null,
            pesquisa: null,
            destaques: [],
            urlimg: "http://admaldeliv.asistemas.com.br/imagens/"
        };
    },
    computed: {
        vpedidos: function() {
            return this.$store.state.pedidos.length;
        }
    },

    created() {
        $(".menu-icon").css("display", "none");
        this.$store.commit("mudapagina", "Categoria");
        Api.get("destaques").then(response => {
            this.destaques = response.data.lista;
        });
        if (this.$route.params.categorias) {
            this.categoria = this.$route.params.categorias;
            this.mudaProdutos(this.categoria);
        } else {
            Api.get("padrao", {}).then(response => {
                this.categoria = response.data.lista.Valor;
                this.mudaProdutos(this.categoria, "padrao");
            });
        }
        Api.get("familias").then(response => {
            this.familias = response.data.lista;
            this.mudaFamiliaSelecionada();
            this.familiascarossel();
        });
    },
    components: {
        Adicionar,
        Metade,
        Botoes
    },

    methods: {
        familiascarossel() {
            var slider = document.querySelector(".items"),
                arrows = document.querySelectorAll(
                    ".wrapper-slider .arrow-left, .wrapper-slider .arrow-right"
                ),
                isDown = false,
                startX,
                scrollLeft;

            slider.scrollLeft = 1970;
            slider.onmousedown = function(e) {
                "use strict";
                isDown = true;
                slider.classList.add("active");
                startX = e.pageX - slider.offsetLeft;
                scrollLeft = slider.scrollLeft;
            };

            slider.onmouseup = function() {
                "use strict";
                isDown = false;
                slider.classList.remove("active");
            };

            slider.onmouseleave = function() {
                "use strict";
                isDown = false;
                slider.classList.remove("active");
            };

            slider.onmousemove = function(e) {
                "use strict";
                if (!isDown) {
                    return;
                }
                e.preventDefault();
                var x = e.pageX - slider.offsetLeft,
                    walk = x - startX;
                slider.scrollLeft = scrollLeft - walk;
            };

            function controlsSlider(num) {
                "use strict";
                var smooth = setInterval(function() {
                    slider.scrollLeft += num;
                }, 10);
                setTimeout(function() {
                    clearInterval(smooth);
                }, 210);
            }
            arrows[0].onclick = function() {
                "use strict";
                controlsSlider(-10);
            };

            arrows[1].onclick = function() {
                "use strict";
                controlsSlider(10);
            };

            window.onkeydown = function(e) {
                "use strict";
                var key = e.keyCode;
                if (key === 39) {
                    controlsSlider(10);
                }
                if (key === 37) {
                    controlsSlider(-10);
                }
            };
        },
        abrirmodal(produto) {
            if (this.$store.state.statusEstabelecimento) {
                if (produto.HoraFinal) {
                    if (
                        produto.HoraFinal == "00:00:00" &&
                        produto.HoraInicio == "00:00:00"
                    ) {
                        if (produto.ProdEspecial == 0) {
                            this.$root.$emit("open-adicionar", produto);
                        } else {
                            this.$root.$emit("open-botoesModal", produto);
                        }
                    } else {
                        var data = new Date();
                        var h = String(data.getHours()) + data.getMinutes(); // 0-23 // 0-59
                        var hora1 = produto.HoraFinal.split(":");
                        var ho =
                            String(hora1[0]) +
                            String(hora1[1]) +
                            String(hora1[2]);
                        if (h > ho) {
                            alert("Produto não está disponivel nesse hórario ");
                            this.mudaProdutos(this.categoria);
                        } else {
                            if (produto.ProdEspecial == 0) {
                                this.$root.$emit("open-adicionar", produto);
                            } else {
                                this.$root.$emit("open-botoesModal", produto);
                            }
                        }
                    }
                } else {
                    if (produto.ProdEspecial == 0) {
                        this.$root.$emit("open-adicionar", produto);
                    } else {
                        this.$root.$emit("open-botoesModal", produto);
                    }
                }
            } else {
                alert("Estabelecimento Fechado");
            }
        },
        mudaProdutos(i, c) {
            Api.post("produtos", {
                categorias: i.replace("%", " ")
            }).then(response => {
                response.data.lista.sort(function(a, b) {
                    return a.NomeProd > b.NomeProd
                        ? 1
                        : b.NomeProd > a.NomeProd
                        ? -1
                        : 0;
                });
                this.todos = response.data.lista;

                this.mudaFamiliaSelecionada();
            });
            if (!c) {
                this.categoria = i.replace(" ", "%");
                this.$router.push(`/cardapio/${i}`);
            }
        },
        mudaFamiliaSelecionada() {
            this.familias.map(c => {
                c.ativo = false;
                if (this.categoria == c.Familia) {
                    c.ativo = true;
                }
            });
        }
        //Ação que será acionada ao apertar o botão + no cardapio,
        // Ela Chama a função Adicionar Num para colocar no Layout a quantidade pedida
        // e sobe para a Store o pedido e sua quantidade
    }
};
export default Categoria;
