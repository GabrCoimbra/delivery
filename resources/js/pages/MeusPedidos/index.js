import Api from "../../axios/api.js";
import Produtos from "../../components/Modal/ProdutosPerfil/ProdutosPerfil.vue";
import moment from "moment";
const Categoria = {
    name: "Categoria",
    data: function () {
        return {
            pedidos: null,
            produtosPedido: null,
            total: "0.0"
        };
    },
    created() {
        this.$store.commit("mudapagina", 'MeusPedidos');
        if (localStorage.token) {
            this.registros();
        } else {
            window.location = "/";
        }
    },
    methods: {
        registros() {
            setInterval(() => {
                Api.post("meusPedidos", {
                    usuario: localStorage.usuario
                }).then(response => {
                    response.data.lista.map(produto => {
                        // 0- Pendente, 1- Confirmado Pelo Estab, 2 - Pedido sendo preparado, 3 - Saiu para Entrega,  4 - Entregue/Finalizado, 5 - Cancelado, 6 -Rejeitado
                        switch (String(produto.StatusPedido)) {
                            case "0":
                                produto.StatusPedido = "Processando...";
                                break;
                            case "1":
                                produto.StatusPedido = "Recebemos seu pedido";
                                break;
                            case "2":
                                produto.StatusPedido = "Pedido sendo preparado";
                                break;
                            case "3":
                                produto.StatusPedido = "Saiu para Entrega";
                                break;
                            case "4":
                                produto.StatusPedido = "Entregue";
                                break;
                            case "5":
                                produto.StatusPedido = "Cancelado";
                                break;
                            case "6":
                                produto.StatusPedido = "Rejeitado";
                                break;
                        }
                        produto.dtPedido = moment(String(produto.dtPedido)).format(
                            "DD/MM/YYYY HH:mm"
                        );
                        produto.dtStatus = moment(String(produto.dtStatus)).format(
                            "DD/MM/YYYY HH:mm"
                        );
                    });
                    this.pedidos = response.data.lista;
                });
            }, 3000)
        }
    },
    beforeDestroy() {
        clearInterval(this.pedidos)
    },
    components: {
        Produtos
    }
};

export default Categoria;
