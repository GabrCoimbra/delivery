import Search from "../../components/Search/Search.vue";
import Api from "../../axios/api.js";

const Cardapio = {
    name: "Cardapio",
    data() {
        return {
            familias: null
        }
    },
    created() {
        this.$store.commit("mudapagina", 'Familias');
        Api.get('familias')
            .then(response => {
                this.familias = response.data.lista
            })
        if ($(window).width() < 767) {
            if (this.familias < 6) {
                $('.wrapper').css({
                    "display": "none"
                })
            } else {

            }
        }
    },
    components: {
        Search
    }
};
export default Cardapio;
