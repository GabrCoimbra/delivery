const Logout = {
    name: "Home",
    created() {
        localStorage.removeItem("token");
        localStorage.removeItem("usuario");
        localStorage.removeItem("telefone");
        localStorage.removeItem("taxa");
        localStorage.removeItem("nome");
        localStorage.removeItem("distancia");
        window.location = "/";
    },
    methods: {}
};
export default Logout;
