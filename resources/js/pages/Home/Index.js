import Api from "../../axios/api.js";
import Login from "../../components/Modal/Login/Login.vue";
const Home = {
    name: 'Home', 
    data () {
        return {
          Login: '',
          Senha: '',
          error: null,
          vlogin:null
        }
      },
      
    computed:{
      verifica: function() {
              if(this.$store.state.vlogin){
                  return this.$store.state.vlogin.split(" ")[0]
              }else{
                  return this.$store.state.vlogin
              }
          }
    },
    components: {
        Login
    },
     methods: {
      }
};
export default Home;
