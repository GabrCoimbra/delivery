import Api from "../../axios/api.js";
import moment from 'moment'
const Categoria = {
    name: "Categoria",
    data: function () {
        return {
            dados: null
        };
    },
    created() {
        this.$store.commit("mudapagina", 'Perfil');
        if (localStorage.token) {
            Api.post('buscausuario', {
                    usuario: localStorage.usuario
                })
                .then(response => {
                    this.dados = response.data.cliente[0]
                })
        }
    }
}

export default Categoria;
