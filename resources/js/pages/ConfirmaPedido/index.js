import ConfirmaPedido from "../../components/Modal/ConfirmaPedido/ConfirmaPedido.vue";
import Login from "../../components/Modal/Login/Login.vue";
import Api from "../../axios/api.js";
const Categoria = {
    name: "Categoria",
    data: function () {
        return {
            produtos: null,
            total: "0.0",
            usuario: localStorage.usuario,
            tx: null,
            distancias: null
        };
    },
    created() {
        $("body")
            .get(0)
            .scrollIntoView();
        if (this.$store.state.qtdProdutos == 0) {
            // window.location = "/";
        }

        $(".menu-icon").css("display", "none");
        this.$store.commit("mudapagina", "ConfirmaPedido");
        this.produtos = this.$store.state.pedidos;
    },
    computed: {
        txentrega: function () {
            var i = this.$store.state.distancia;
            Api.get("taxaentrega").then(p => {
                this.$store.commit(
                    "mudataxa",
                    this.calculatx(p.data.lista, this.$store.state.distancia)
                );
            });
            return parseFloat(this.$store.state.taxa);
        },
        verifica: function () {
            return this.$store.state.vlogin;
        },
        funcTotal: function () {
            this.total = "0.0";
            var p = this.$store.state.pedidos.map(produto => {
                if (produto.qtd == 0.5) {
                    if (this.$store.state.especial == 1) {
                        this.total =
                            parseFloat(this.total) +
                            parseFloat(produto.preco) * 0.5;
                    } else {
                        if (produto.itemMaior == 1) {
                            this.total =
                                parseFloat(this.total) +
                                parseFloat(produto.preco) * 1;
                        }
                    }
                } else {
                    this.total =
                        parseFloat(this.total) +
                        parseFloat(produto.preco) * produto.qtd;
                }
                // Se existe adicional
                if (produto.adc) {
                    produto.adc.map(a => {
                        if (produto.qtd != 0.5) {
                            this.total =
                                this.total + parseFloat(a.preco) * a.qtd;
                        } else {
                            if (this.$store.state.adc == 1) {
                                this.total =
                                    parseFloat(this.total) +
                                    parseFloat(
                                        parseFloat(a.preco) * a.qtd * 0.5
                                    );
                            } else {
                                this.total =
                                    parseFloat(this.total) +
                                    parseFloat(parseFloat(a.preco) * a.qtd);
                            }
                        }
                    });
                }
            });
            return parseFloat(this.total).toLocaleString("pt-BR", {
                style: "currency",
                currency: "BRL"
            });
        },
        totalpedido: function () {
            if (this.txentrega) {
                var t = parseFloat(this.total) + parseFloat(this.txentrega);
                return parseFloat(t).toLocaleString("pt-BR", {
                    style: "currency",
                    currency: "BRL"
                });
            } else {
                return null;
            }
        }
    },
    methods: {
        voltar() {
            history.back();
        },

        //Ação que será acionada ao apertar o botão + no cardapio,
        // Ela Chama a função Adicionar Num para colocar no Layout a quantidade pedida
        // e sobe para a Store o pedido e sua quantidade
        adicionarProduto(produto) {
            this.adicionarNum(produto);
        },
        calculatx(tx, d) {
            var a = 0;
            var i = false;
            tx.map(p => {
                if (p.DistanciaMetros == 0) {
                    a = parseFloat(p.Taxa);
                    i = true;
                } else {
                    if (!i) {
                        if (parseInt(p.DistanciaMetros) >= parseInt(d)) {
                            a = p.Taxa;
                        }
                    }
                }
            });
            return a;
        },

        adicionarNum(p) {
            this.produtos.map(produto => {
                if (parseInt(produto.id) === parseInt(p.id)) {
                    if (produto.qtd === 0) {} else {
                        this.$store.commit("adicionarQtd", produto.id);
                    }
                }
            });
        },
        retiraAdc(produto, adc) {
            var i = {
                produto: produto.id,
                adicional: adc.id
            };
            this.$store.commit("retiraAdc", i);
        },

        retiraObs(produto, obs) {
            var i = {
                produto: produto.id,
                obs: obs.nome
            };
            this.$store.commit("retiraObs", i);
        },
        retiraMeia(meia) {
            this.$store.commit("retiraProdutoMeia", meia.especial);
        },

        //Ação que será acionada ao apertar o botão - no cardapio,
        // Ela Chama a função retira Num para retirar no Layout a quantidade pedida
        // e retira na Store o pedido ou sua quantidade
        retiraProduto(produto) {
            this.retiraNum(produto);
        },
        retiraNum(p) {
            this.produtos.map(produto => {
                if (parseInt(produto.id) === parseInt(p.id)) {
                    var qtd = produto.qtd;
                    if (qtd == 1) {
                        this.$store.commit("retiraProduto", produto.id);
                    } else {
                        this.$store.commit("retiraQtd", produto.id);
                    }
                }
            });
        }
    },
    components: {
        ConfirmaPedido,
        Login
    }
};

export default Categoria;
