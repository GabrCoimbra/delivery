import Api from "../../axios/api.js";

const Mesas = {
    name: "Mesas",
    data() {
        return {
            mesas: ''
        }
    },
    created() {
        if (!localStorage.token) {
            window.location = "/";

        }
        Api.get('mesas')
            .then(response => {
                this.mesas = response.data.lista
            })
    },

    components: {},

    methods: {
        mesa: function (m, c) {
            var mesa = [m, c]
            this.$store.commit("mudaMesa", mesa);
            this.$router.push("/cardapio");
        }
    }
};
export default Mesas;
