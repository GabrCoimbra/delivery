import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);
import router from "./router";
import store from "./storage/index";

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
