import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        vlogin: localStorage.nome,
        pedidos: [],
        metades: 0,
        qtdMetades: 0,
        qtdProdutos: 0,
        distancia: localStorage.distancia,
        pag: null,
        cpf: localStorage.cpf,
        especial: null,
        taxa: null,
        adc: null,
        statusEstabelecimento: null
    },
    mutations: {
        mudarCobranca(state, c) {
            state.especial = parseInt(c.especial);
            state.adc = parseInt(c.adc);
        },
        mudastatus(state, s) {
            state.statusEstabelecimento = s;
        },
        mudapagina(state, pag) {
            state.pag = pag;
        },
        mudalogin(state, r) {
            state.vlogin = r.nome;
            state.cpf = r.cpf;
        },
        mudataxa(state, t) {
            state.taxa = t;
        },
        mudadistancia(state, t) {
            state.distancia = t;
        },
        adicionarProduto(state, pedido) {
            if (pedido.Contagem == 0) {
                pedido.Contagem = 1;
            }
            var p = {
                id: pedido.id,
                idproduto: pedido.idProduto,
                nome: pedido.NomeProd,
                preco: pedido.PrcVenda,
                qtd: pedido.Contagem,
                obs: pedido.obs,
                adc: pedido.adc
            };
            state.pedidos.push(p);
            state.qtdProdutos = state.qtdProdutos + 1;
        },
        retiraAdc(state, i) {
            state.pedidos.map(produto => {
                if (parseInt(produto.id) === parseInt(i.produto)) {
                    produto.adc.map(adc => {
                        if (adc.id == i.adicional) {
                            if (adc.qtd > 1) {
                                adc.qtd = adc.qtd - 1;
                            } else {
                                var idretirar = produto.adc.findIndex(
                                    a => parseInt(a.id) == parseInt(i.adicional)
                                );
                                produto.adc.splice(idretirar, 1);
                            }
                        }
                    });
                }
            });
        },
        retiraObs(state, i) {
            state.pedidos.map(produto => {
                if (parseInt(produto.id) === parseInt(i.produto)) {
                    var idretirar = produto.obs.findIndex(
                        a => parseInt(a.nome) == parseInt(i.obs)
                    );
                    produto.obs.splice(idretirar, 1);
                }
            });
        },
        adicionarMetade(state, pedido) {
            var p = {
                id: pedido.id,
                idproduto: pedido.idProduto,
                nome: pedido.NomeProd,
                preco: pedido.PrcVenda,
                qtd: pedido.Contagem,
                especial: pedido.idPrdEspecial,
                obs: pedido.obs,
                adc: pedido.adc
            };
            if (pedido.itemMaior) {
                p.itemMaior = pedido.itemMaior;
            }

            state.pedidos.push(p);
            state.qtdProdutos = state.qtdProdutos + 1;
        },
        retiraProdutoMeia(state, meia) {
            var index = state.pedidos.findIndex(
                produto => parseInt(produto.especial) == parseInt(meia)
            );
            state.pedidos.splice(index, 1);
            var index2 = state.pedidos.findIndex(
                produto => parseInt(produto.especial) == parseInt(meia)
            );
            state.pedidos.splice(index2, 1);
        },
        addNumMetade(state) {
            state.qtdMetades = state.qtdMetades + 1;
        },
        adicionarQtd(state, id) {
            state.pedidos.map(produto => {
                if (parseInt(produto.id) === parseInt(id)) {
                    produto.qtd = parseInt(produto.qtd) + 1;
                }
            });
        },

        adicionarObs(state, id) {
            state.pedidos.map(produto => {
                if (parseInt(produto.id) === parseInt(produto.id)) {
                    produto.obs = id.obs;
                }
            });
        },
        retiraProduto(state, id) {
            var index = state.pedidos.findIndex(
                produto => parseInt(produto.id) == parseInt(id)
            );
            state.pedidos.splice(index, 1);
        },
        retiraQtd(state, id) {
            state.pedidos.map(produto => {
                if (parseInt(produto.id) === parseInt(id)) {
                    produto.qtd = parseInt(produto.qtd) - 1;
                }
            });
        }
    }
});
