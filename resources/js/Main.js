import Header from "./components/Header/Header.vue";

import Menu from "./components/Menu/Menu.vue";
const App = {
    data: function() {
        return { pag: null };
    },
    components: {
        Header,
        Menu
    },
    created() {
        this.pag = this.$router.currentRoute.name;
    }
};
export default App;
