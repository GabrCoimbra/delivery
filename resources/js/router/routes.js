import Home from "../pages/Home/Home.vue";
import Mesas from "../pages/Mesas/Mesas.vue";
import Cardapio from "../pages/Cardapio/Cardapio.vue";
import Categorias from "../pages/Categorias/Categorias.vue";
import ConfirmaPedido from "../pages/ConfirmaPedido/ConfirmaPedido.vue";
import MeusPedidos from "../pages/MeusPedidos/MeusPedidos.vue";
import Logout from "../pages/Logout/Logout.vue";
import Perfil from "../pages/Perfil/Perfil.vue";

const routes = [{
        path: "/",
        name: "Home",
        component: Categorias,

        meta: {
            title: "Home",
            guestOnly: true
        }
    },
    {
        path: "/mesas",
        name: "mesas",
        component: Mesas,

        meta: {
            title: "Mesas",
            guestOnly: true
        }
    },
    {
        path: "/familias",
        name: "familias",
        component: Cardapio,

        meta: {
            title: "Familias",
            guestOnly: true
        }
    },
    {
        path: "/cardapio/:categorias",
        name: "Categorias",
        component: Categorias,

        meta: {
            title: "cardapio",
            guestOnly: true
        }
    },
    {
        path: "/confirmapedido",
        name: "ConfirmaPedido",
        component: ConfirmaPedido,

        meta: {
            title: "ConfirmaPedido",
            guestOnly: true
        }
    },
    {
        path: "/meusPedidos",
        name: "meusPedidos",
        component: MeusPedidos,

        meta: {
            title: "meusPedidos",
            guestOnly: true,
            requiresAuth: true,

        }
    },
    {
        path: "/logout",
        name: "logout",
        component: Logout,

        meta: {
            title: "logout",
            guestOnly: true,
            requiresAuth: true,

        }
    },
    {
        path: "/perfil",
        name: "perfil",
        component: Perfil,

        meta: {
            title: "perfil",
            guestOnly: true,
            requiresAuth: true,

        }
    }
];

export default routes;
//{ path: '/horario/:dia/:hora', component: Bar },
//    console.log(this.$route.params.dia);
