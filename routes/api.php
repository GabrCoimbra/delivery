<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::get('/padrao','ConfigController@Padrao');
Route::get('/taxaentrega','ConfigController@TaxaEntrega');
Route::get('/horarios','ConfigController@HorarioFuncionamento');
Route::get('/hrentrega','ConfigController@TempoEntrega');
Route::get('/tipocobranca','ConfigController@Cobranca');


Route::get('/buscacartoes', 'CartoesController@buscaCartoes');
Route::post('/login', 'Api\AuthController@login');
Route::post('/cadastro', 'Api\AuthController@cadastro');
Route::post('/buscaentrega', 'ClienteController@VerificaEntrega');
Route::get('/familias','FamiliaController@Listar');
Route::get('/destaques','FamiliaController@Destaques');
Route::post('/produtos','FamiliaController@listarProdutos');
Route::post('/buscaprodutos','FamiliaController@buscaProdutos');
Route::post('/buscametades','FamiliaController@buscaMetades');
Route::post('/observacoes','PedidoController@ObsProduto');
Route::post('/adicionais','PedidoController@Adicional');
Route::post('/infprodutos','PedidoController@InfProdutos');
Route::middleware('auth:api')->post('/enviaPedido','PedidoController@SalvarPedido');
Route::middleware('auth:api')->post('/meusPedidos','PedidoController@MeusPedidos');
Route::middleware('auth:api')->post('/produtospedido','PedidoController@ProdutosPedido');
Route::middleware('auth:api')->post('/buscausuario','ClienteController@BuscaCliente');
Route::middleware('auth:api')->post('/buscacupom','CupomController@BuscaCupom');
