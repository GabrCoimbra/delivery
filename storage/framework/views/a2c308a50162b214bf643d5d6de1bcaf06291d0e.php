<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Delivery - Alcance Soluções em Sistemas</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script src=" https://code.jquery.com/jquery-3.4.1.min.js"> </script>


        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://kit.fontawesome.com/457dd627d2.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!-- Script for polyfilling Promises on IE9 and 10 -->
        <script src='https://cdn.polyfill.io/v2/polyfill.min.js'></script>

        <!-- script -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </head>
    <body>
        <div id="app"></div>
        <script src="/js/app.js"></script>
        <script>
            /*global $, console*/
            
        $(document).ready(function(){
                $('.icone-login').click(function() {
                    $('.menu-icon').toggle(0);
                    var a = $('.menu-icon').attr('data-state')
                    a = 1
                    
                })
                $(document).scroll(function () {
                    $('.menu-icon').css('display','none');
                    // oscultador de scroll
                    var posicaoScroll = $(document).scrollTop(); // obtem a quantidade de scroll no momento
                    var tamanho = ($('.horario-logo')[0].clientHeight)/3
                    
                    if (posicaoScroll > 10) {
                        if($(window).width() < 767){
                            
                            $(".items").css("position", "fixed");
                            $(".items").css("top", "60px");
                        }
                        $(".horario-logo").css("position", "fixed");
                        $(".logo").css("display", "none");
                        $(".nome-empresa").css("display", "block");
                    }
                    else{
                        if($(window).width() < 767){
                            
                            $(".items").css("position", "relative");
                            $(".items").css("top", "0");
                        }
                        $(".logo").css("display", "block");
                        $(".nome-empresa").css("display", "none");
                    }
            })
        })



        </script>
        
    </body>
</html>
<?php /**PATH C:\Users\gab4_\Desktop\cardapio - Cópia\resources\views/welcome.blade.php ENDPATH**/ ?>