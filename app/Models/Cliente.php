<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class Cliente extends Authenticatable implements JWTSubject
{
    public $timestamps = false;
    protected $table = 'Clientes';
    protected $primaryKey = 'idCliente';
    
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public function setPasswordAttribute($password)
    {
        if ( !empty($password) ) {
            $this->attributes['password'] = $password;
        }
    }    
    public function login($user,$senha){

    }
    
}
