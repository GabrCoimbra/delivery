<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class Pedidos extends Model
{
    public $timestamps = false;
    protected $table = 'Pedidos';
    protected $primaryKey = 'idPedido';
    
}
