<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\Cupom;

class CupomController extends Controller
{
    public function BuscaCupom(Request $request){
        setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8');
        date_default_timezone_set('America/Sao_Paulo');
        $cupom = Cupom::where('Nome',$request->cupom)
        ->where('Ativo','1')
        ->first();
        if(empty($cupom)){
            return response()->json(['error'=> 'Cupom inválido ou inativo']);
        }
        else{
            $data = date('Y-m-d');
            if(empty($cupom->DataFinal) || strtotime($cupom->DataFinal) > strtotime($data)){
                $uso = DB::table('promoutiliza')
                ->where('idPromocao',$cupom->idPromocao)
                ->where('idCliente',$request->id)
                ->count();
                if($cupom->QtdLimiteCli == 0 || $uso < $cupom->QtdLimiteCli){
                    return response()->json(compact('cupom'));
                }
                else{
                    return response()->json(['error'=> 'Limite de uso atingido']);
                }
            }
            else{
                    return response()->json(['error'=> 'Cupom invalido pela data de uso']);
                
            }
        }
        
    }
}
