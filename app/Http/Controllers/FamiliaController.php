<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\Familias;

class FamiliaController extends Controller
{
    public function listar(){
        $lista = DB::table('produtos')
        ->select('familias.*')
        ->leftJoin('familias','produtos.Familia','=','familias.Familia')
        ->where('familias.Familia','!=',"ADICIONAIS")
        ->orderBy(\DB::raw('-`Prioridade`'), 'DESC')
        ->orderBy('Familia', 'ASC')
        ->groupBy('produtos.Familia')
        ->having(\DB::raw('Count(`produtos`.`idProduto`)'),'>','0')
        ->get();
        return response()->json(compact('lista'));
    }
    public function listarProdutos( Request $request){
        setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8');
        date_default_timezone_set('America/Sao_Paulo');

        $itensComRestricaoTotal = DB::table('produtosdiahora')
        ->select('idProduto')
        ->groupBy('idProduto')
        ->get();
        $data = array();
        foreach ($itensComRestricaoTotal as $i) {
            $data[] = $i->idProduto;
        }
        $itensSemRestricao = Familias::select('produtos.*','prodinfocompl.Descricao','fotos.*')
        ->whereNotIn('produtos.idProduto',$data)
        ->where('Familia',$request->categorias)
        ->where('adicional',"0")
        ->leftJoin('prodinfocompl','prodinfocompl.idProdCompl','=','produtos.idProduto')
        ->leftJoin('fotosProduto','fotosProduto.idProduto','=','produtos.idProduto')
        ->leftJoin('fotos','fotos.idFoto','=','fotosProduto.idFoto')
        ->orderBy('produtos.NomeProd')
        ->get();
        $dia = date('N');
        $itensComRestricao = Familias::select('produtos.*','prodinfocompl.Descricao','fotos.*',
        'produtosdiahora.HoraInicio','produtosdiahora.HoraFinal')
        ->where('produtos.Familia',$request->categorias)
        ->where('adicional',"0")
        ->where(function ($q) {
            $q->where(function ($r) {
                $hora = date('H:i:s');
                $r->where('produtosdiahora.HoraInicio','<',$hora);
                $r->where('produtosdiahora.HoraFinal','>',$hora);
            });
            $q->orWhere('produtosdiahora.HoraInicio','00:00:00');
        })
        ->where('produtosdiahora.DiaSemana',$dia)
        ->where('produtosdiahora.Ativo','1')
        ->rightJoin('produtosdiahora','produtosdiahora.idProduto','produtos.idProduto')
        ->leftJoin('prodinfocompl','prodinfocompl.idProdCompl','=','produtos.idProduto')
        ->leftJoin('fotosProduto','fotosProduto.idProduto','=','produtos.idProduto')
        ->leftJoin('fotos','fotos.idFoto','=','fotosProduto.idFoto')
        ->orderBy('produtos.NomeProd')
        ->get();
        $lista = $itensComRestricao->merge($itensSemRestricao);
        return response()->json(compact('lista'));
    }
    public function Destaques( ){
        setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8');
        date_default_timezone_set('America/Sao_Paulo');

        $itensComRestricaoTotal = DB::table('produtosdiahora')
        ->select('idProduto')
        ->groupBy('idProduto')
        ->get();
        $data = array();
        foreach ($itensComRestricaoTotal as $i) {
            $data[] = $i->idProduto;
        }
        $itensSemRestricao = Familias::select('produtos.*','prodinfocompl.Descricao','fotos.*')
        ->where('Destaque','1')
        ->whereNotIn('produtos.idProduto',$data)
        ->where('adicional',"0")
        ->leftJoin('prodinfocompl','prodinfocompl.idProdCompl','=','produtos.idProduto')
        ->leftJoin('fotosProduto','fotosProduto.idProduto','=','produtos.idProduto')
        ->leftJoin('fotos','fotos.idFoto','=','fotosProduto.idFoto')
        ->orderBy('produtos.NomeProd')
        ->get();
        $dia = date('N');
        $itensComRestricao = Familias::select('produtos.*','prodinfocompl.Descricao','fotos.*',
        'produtosdiahora.HoraInicio','produtosdiahora.HoraFinal')
        ->where('Destaque','1')
        ->where('adicional',"0")
        ->where(function ($q) {
            $q->where(function ($r) {
                $hora = date('H:i:s');
                $r->where('produtosdiahora.HoraInicio','<',$hora);
                $r->where('produtosdiahora.HoraFinal','>',$hora);
            });
            $q->orWhere('produtosdiahora.HoraInicio','00:00:00');
        })
        ->where('produtosdiahora.DiaSemana',$dia)
        ->where('produtosdiahora.Ativo','1')
        ->rightJoin('produtosdiahora','produtosdiahora.idProduto','produtos.idProduto')
        ->leftJoin('prodinfocompl','prodinfocompl.idProdCompl','=','produtos.idProduto')
        ->leftJoin('fotosProduto','fotosProduto.idProduto','=','produtos.idProduto')
        ->leftJoin('fotos','fotos.idFoto','=','fotosProduto.idFoto')
        ->orderBy('produtos.NomeProd')
        ->get();
        $lista = $itensComRestricao->merge($itensSemRestricao);
        return response()->json(compact('lista'));
    }
    public function buscaProdutos( Request $request){
        $lista = Familias::where('NomeProd', 'like', '%'.$request->nome.'%')
        ->orderBy('produtos.NomeProd')
        ->get();
        return response()->json(compact('lista'));
    }
    
    public function buscaMetades( Request $request){
        $lista = Familias::where('ProdEspecial', '1')
        ->where('Familia',"=",$request->familia)
        ->where('adicional',"0")
        ->leftJoin('prodinfocompl','prodinfocompl.idProdCompl','=','produtos.idProduto')
        ->orderBy('produtos.NomeProd')
        ->get();
        return response()->json(compact('lista'));
    }
   
}
