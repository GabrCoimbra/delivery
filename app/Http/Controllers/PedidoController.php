<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\DB;
use App\Models\Pedidos;
use App\Models\ItensPedido;
use App\Models\Familias;
use App\Models\Observacoes;
use App\Models\Cliente;
use App\Models\CupomUtiliza;

class PedidoController extends Controller
{
    public function listar(){
        $lista = Mesas::groupBy('NumComanda')->get();
        return response()->json(compact('lista'));
    }
    public function InfProdutos( Request $request){
        $adc = $this->Adicional($request->produto);
        $obs = $this->ObsProduto($request->produto);
        return response()->json(compact('obs','adc'));
    }
    function ObsProduto( $produto ){
        $lista = DB::table('obsprodutos')
            ->where('obsprodutos.IdProduto',$produto)
            ->join('observacoes', 'observacoes.idObservacao', '=', 'obsprodutos.idObservacao')
            ->select('obsprodutos.*', 'observacoes.*')
            ->get();
        return $lista;
    }
    function Adicional( $produto ){
        $lista = DB::table('adicionais')
            ->where('adicionais.idProduto',$produto)
            ->join('produtos', 'produtos.idProduto', '=', 'adicionais.idAdicional')
            ->select('adicionais.*', 'produtos.idProduto', 'produtos.NomeProd','produtos.PrcVenda')
            ->get();
        return $lista;
    }
    public function ProdutosPedido( Request $request ){
        $lista = DB::table('ItensPedido')
            ->where('ItensPedido.idPedido',$request->pedido)
            ->join('produtos', 'produtos.idProduto', '=', 'ItensPedido.idProduto')
            ->select('ItensPedido.*', 'produtos.NomeProd','produtos.PrcVenda')
            ->get();
        return response()->json(compact('lista'));
    }
    public function SalvarPedido( Request $request){
        setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8');
        date_default_timezone_set('America/Sao_Paulo');
        
        $pedido = new Pedidos;
        $pedido->idCliente = $request->usuario;
        $pedido->dtPedido =  date("Y-m-d H:i:s");
        
        if(strval($request->FormaPagto) != 'cartao'){
            
            $pedido->FormaPagto = $request->FormaPagto;
        }
        else{
            $pedido->BandeiraCT = $request->bandeira;
            $b = DB::table('bandeira')
            ->where('Bandeira',$request->bandeira)
            ->first();
            $pedido->FormaPagto = $b->TipoCT == "D" ? 3 : 7;
        }
        $pedido->Troco = $request->Troco;
        $pedido->CPFNota = $request->CPFNota;
        if(!empty($request->CPFNota)){
            $user = Cliente::select()
            ->where('IdCliente',$request->usuario)
            ->update(['CpfCliente' => $request->CPFNota]);
        }
        $pedido->MotivoCanc = null;
        $pedido->StatusPedido = '100';
        $pedido->Viagem = $request->entrega;
        if($request->entrega == 0){
            $pedido->TxEntrega = '0';
        }
        else{
            $pedido->TxEntrega = $request->txentrega;
        }
        $pedido->ValDesconto = $request->desconto;
        $pedido->save();
        $id = $pedido->idPedido;
        if($request->cupom){
            $cupom = new CupomUtiliza;
            $cupom->idpromocao = $request->cupom;
            $cupom->idVenda = $id;
            $cupom->idCliente = $request->usuario;
            $cupom->save();
        }
        $v = false;
        foreach( $request->data['pedidos'] as $a){
            $produto = Familias::where('idProduto',$a['idproduto'])->first();
            $data = new ItensPedido;
            $data->idPedido = $id;
            $data->idProduto = $produto['idProduto'];
            $data->prcProduto = $produto['PrcVenda'];
            $data->cusProduto = $produto['CustoAtual'];
            if(isset($a['especial'])){
                $data->idPrdEspecial = $a['especial'];
            }
            $data->qtdProduto =  $a['qtd'];
            $data->idAdicional =  null ;
            if($data->save()){
                $v = true;
                if(isset($a['adc'])){
                    foreach($a['adc'] as $adcio){
                        for ($i=1; $i <= $adcio['qtd']; $i++){        
                            $produtos = Familias::where('idProduto',$adcio['id'])->first();
                            $adc = new ItensPedido;
                            $adc->idPedido = $id;
                            $adc->idProduto = $produtos['idProduto'];
                            $adc->prcProduto = $produtos['PrcVenda'];
                            $adc->cusProduto = $produtos['CustoAtual'];
                            $adc->qtdProduto =  1;
                            if(isset($a['especial'])){
                                $adc->idPrdEspecial = $a['especial'];
                            }
                            $adc->idAdicional =  $data->idItensPedido ;
                            $adc->save();
                        }
                    }
                }
            }
            else{
                $v = false;
            }
            if(!empty($a['obs'])){
                $lacoObs = $a['obs'];

                foreach( $lacoObs as $s){
                    $o = new Observacoes;
                    $o->idItem = $data->idItensPedido;
                    if(!empty($s['id'])){
                        $o->idObservacao = $s['id']; 
                    }
                    else{
                        $o->idObservacao = null;
                    }
                    $o->Observacao = $s['nome'];
                    $o->NumVenda = $id;
                    $o->save();
                }
            }
            
        }
        if($v){
            $pedido->StatusPedido = '0';
            $pedido->save();
            return response()->json(['msg'=> 'Sucesso ao fazer o pedido']);
        }
        else{
            return response()->json(['error'=> 'Falha ao fazer o pedido']);
        }
    }
    public function MeusPedidos( Request $request){
        
        $lista = Pedidos::where('idCliente',$request->usuario)
        ->orderby('dtPedido','DESC')
        ->get();
        return response()->json(compact('lista'));
    }
    
}
