<?php

namespace App\Http\Controllers\Api;


use Hash;
use DB;

use Pnlinh\GoogleDistance\Facades\GoogleDistance;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use App\Models\Cliente;
use App\Models\Parametros;
use GuzzleHttp\Client;

class AuthController extends Controller
{
    private $jwtAuth;
    public function __construct(JWTAuth $jwtAuth){
        $this->jwtAuth = $jwtAuth;
    }

    public function login(Request $request)
    {
        // grab credentials from the request
        if(isset($request->Url)){
            $user = Cliente::select()
                        ->where('FoneCli',$request->Login)
                        ->first();
        }
        else{
            $user = Cliente::select()
            ->orwhere('Email',$request->Login)
            ->orwhere('CpfCliente',$request->Login)
            ->orwhere('FoneCli',$request->Login)
            ->first();
        }
        if (!empty($user)){
                    $token = auth()->setTTL(43800)->login($user);
                    $retorno = [
                        'token' => $token,
                        'usuario' => $user->IdCliente,
                        'nome' => $user->NmCliente,
                        'telefone' => $user->FoneCli,
                        'distancia' => $user->DistanciaEntrega,
                        'cpf' => $user->CpfCliente
                    ];
                    return response()->json(compact('retorno'));
        }
        $msg = ['msg' => 'Usuario ou senha invalido'];
        
        return response()->json(compact('msg'));
        // all good so return the token
        
    }
    public function cadastro(Request $request){
        if(isset($request->dados[3])){
            $user = Cliente::select()->where('CpfCliente',$request->dados[3])->first();
            if($user){
                $error = "Cpf já cadastrado";
                return response()->json(compact('error'));
            }
        }
        if(isset($request->dados[5])){
            $user = Cliente::select()->where('FoneCli',$request->dados[5])->first();
            if($user){
                $error = "Celular já cadastrado";
                return response()->json(compact('error'));
            }
        }
        $cliente = new Cliente;
        $cliente->NmCliente = $request->dados[0];
        $cliente->CpfCliente = $request->dados[3];
        $cliente->dtNascimento = $request->dados[1];
        $cliente->GeneroCliente = $request->dados[2];
        $cliente->Email = $request->dados[4];
        $cliente->FoneCli = $request->dados[13].$request->dados[5];
        $cliente->CepCliente = $request->dados[6];
        $cliente->endCliente = $request->dados[10];
        $cliente->NumEndCliente = $request->dados[11];
        $cliente->ComplEndCliente = $request->dados[12];
        $cliente->BaiCliente = $request->dados[9];
        $cliente->CidCliente = $request->dados[8];
        $cliente->EstadoCliente = $request->dados[7];
        $cliente->StatusCli = '0';
        $txfixa = DB::table('entregadlvtaxa')
        ->where('DistanciaMetros')
        ->get();
        if(!empty($txfixa)){
            $num = Parametros::where('Nome','NUMENDERECO')
            ->first();
            $rua = Parametros::where('Nome','ENDERECO')
            ->first();
            $municipio = Parametros::where('Nome','MUNICIPIO')
            ->first();

            $distancia = $this->BuscaValidadeCep($request->dados[8],$request->dados[10],$request->dados[11],$cliente->EstadoCliente);
            $cliente->DistanciaEntrega = $distancia->rows[0]->elements[0]->distance->value;
        }
        $limiteAtende = DB::table('entregadlvtaxa')
                ->orderBy('DistanciaMetros','DESC')
                ->first();
        if(empty($limiteAtende->DistanciaMetros) || $limiteAtende->DistanciaMetros > $cliente->DistanciaEntrega){
            if($cliente->save()){
                $token = auth()->setTTL(43800)->login($cliente);
                        $retorno = [
                            'token' => $token,
                            'usuario' => $cliente->idCliente,
                            'nome' => $cliente->NmCliente,
                            'telefone' => $cliente->FoneCli,
                            'distancia' => $cliente->DistanciaEntrega,
                            'cpf' => $cliente->CpfCliente
                        ];
                return response()->json(compact('retorno'));
            } 
        }
        else{
                $error = "Área não atendida";
                return response()->json(compact('error'));
        }
        
        
    }
    public function logout (Request $request){
        Funcionarios::where('Login', $request->Login)
                    ->update(['Logado' => 0]);
        $token = $this->jwtAuth->getToken();
        $this->jwtAuth->invalidate($token);
        return response()->json(['logout']);    
    }
    
    function BuscaValidadeCep($cidade,$endereco,$numero,$estado){
        $num = Parametros::where('Nome','NUMENDERECO')
        ->first();
        $rua = Parametros::where('Nome','ENDERECO')
        ->first();
        $municipio = Parametros::where('Nome','MUNICIPIO')
        ->first();
        $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins="Brasil,'.$municipio->Valor.','.$rua->Valor.', '.$num->Valor.'"&destinations="Brasil,'.$estado.','.$cidade.','.$endereco.','.$numero.'"&key=AIzaSyAB1Sy0fo7F5CxJe-WgTjTaFPpUB9DFGMg';
        $client = new Client();
    	$response = $client->request('GET', $url, ['verify' => false]);
    	$statusCode = $response->getStatusCode();
    	$body = $response->getBody()->getContents();

    	return json_decode($body);
    }
}
