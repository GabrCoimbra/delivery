<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Config;

class ConfigController extends Controller
{
    public function Padrao(){
        $lista = Config::where('Nome','FAMILIAPADRAO')
        ->first();
        
        return response()->json(compact('lista'));
    }
    
    public function Cobranca(){
        $especial = Config::where('Nome','ESPECIALMEDIA')
        ->first();
        $adc = Config::where('Nome','ADICMEDIA')
        ->first();
        $lista = [
            'especial' => $especial->Valor,
            'adc' => $adc->Valor
        ];
        return response()->json(compact('lista'));
    }
    public function TempoEntrega(){
        $lista = Config::where('Nome','PRAZOMEDENTREGA')
        ->first();
        return response()->json(compact('lista'));
    }
    public function TaxaEntrega(Request $request){
        $verifica = Config::where('Nome','MODOENTREGA')
        ->where('Valor','!=','AUTO')
        ->first();
        if(empty($verifica)){
                $lista = DB::table('entregadlvtaxa')
                ->orderBy('DistanciaMetros','DESC')
                ->get();
            
        }
        else{
            $txfixa = Config::where('Nome','TXENTREGA')
            ->first();
            $lista = array();
            $txfixa->Valor = str_replace(",", ".", $txfixa->Valor); 
            array_push($lista,[
                'DistanciaMetros'=> 0,
                'Taxa' =>   $txfixa->Valor
            ]);
        }
        return response()->json(compact('lista'));
    }
    public function HorarioFuncionamento(Request $request){
        setlocale(LC_TIME, 'pt_BR.utf-8', 'pt_BR.utf-8', 'pt_BR.utf-8');
        date_default_timezone_set('America/Sao_Paulo');
        $dia = strftime('%A', strtotime('today'));
        switch ($dia){
            case 'sábado':
            break;
            case 'domingo':
            break;
            default:
                $dia = $dia.'-feira';

        }
        $ativo = Config::where('Nome','ROBOFUNC')
        ->first();
        if(!empty($ativo)){
            $d = date('Y-m-d',strtotime($ativo->Valor));
            $hora = date('H:i:s',strtotime('+5 minutes', strtotime($ativo->Valor)));
            if(strtotime($d) == strtotime(date('Y-m-d'))){
                if(strtotime($hora) > strtotime(date('H:i:s')) ){
                        $lista = DB::table('funcionamentohora')
                                    ->where('DiaSemana','Like',$dia)
                                    ->where('Ativo',1)
                                    ->orderby('HoraInicio','ASC')
                                    ->get();
                }
                else{
                    $lista = "";
                }
            }
            else{
                $lista  = "";
            }
            
        }
        else{
            $lista= "";
        }

        return response()->json(compact('lista'));
    }
}
