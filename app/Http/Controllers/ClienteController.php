<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use App\Models\Cliente;

class ClienteController extends Controller
{
    public function BuscaCliente( Request $request){
        $cliente = Cliente::where('IdCliente',$request->usuario)->get();
        
        return response()->json(compact('cliente'));
    }
    public function VerificaEntrega( Request $request){
        $mensagem = "";
        $cidade = DB::table('entregadlvatende')
            ->select('Atende')
            ->where('Cidade',$request->cidade)
            ->where('Bairro',null)
            ->where('CEP',null)
            ->first();
        if(isset($cidade->Atende) && $cidade->Atende == 1){
            $bairro = DB::table('entregadlvatende')
            ->select('Atende')
            ->where('Cidade',$request->cidade)
            ->where('Bairro',$request->bairro)
            ->where('CEP',null)
            ->first();
            if($bairro == null || $bairro->Atende == 1 ){
                 $cep = DB::table('entregadlvatende')
                    ->select('Atende')
                    ->where('Cidade',$request->cidade)
                    ->where('Bairro',$request->bairro)
                    ->where('CEP',$request->cep)
                    ->first();
                    if($cep == null || $cep->Atende == 1 ){
                        $mensagem = "1";
                    }
                    else{
                        $mensagem = "0";
                    }
            }
            else{
                $mensagem = "0";
            }
        }
        else{
            $mensagem = "0";
        }
         return response()->json(compact('mensagem'));
    }
}
